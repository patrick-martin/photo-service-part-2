import { getImagesResponse, postImageResponse } from "../apiStubs";
//this is just stub/dummy data
//we will be talking to a third-party api and sending back actual data

// () => Promise(getImagesResponse)
// export const getImages = () => {
//   console.log("getImages function fired!");
//   return new Promise((resolve, reject) => {
//     resolve(getImagesResponse);
//   });
// };

// // () => Promise(postImageResponse)
// export const postImage = (formData) => {
//   console.log("postImage function fired!");
//   return new Promise((resolve, reject) => {
//     resolve(postImageResponse);
//   });
// };

export const getImages = (req, res) => {
  console.log("getImages function fired!");
  res.send(getImagesResponse);
};

export const postImage = (req, res) => {
  console.log("postImage function fired!");
  res.send(postImageResponse);
};
